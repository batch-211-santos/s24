console.log("Hello World!");


// A

let num = 3
let getCube = num ** 3;


let message = `The cube of ${num} is ${getCube}.`

console.log(message);



// B

const address = ["258 Washington Ave NW", "California", 90011];

const [streetAddress, state, postCode] = address;
console.log(`I live at ${streetAddress}, ${state} ${postCode}.`)



// C

let animal = {
	name: "Lolong",
	weight: "1075 kgs",
	length: "20 ft 3 in"
}


const {name, weight, length} = animal;


let message3 = `${name} was a saltwater crocodile. He weighed at ${weight} with a measurement of ${length}.`
console.log(message3);



// D

const numbers = [1, 2, 3, 4, 5];


numbers.forEach((number) => {
	console.log(number)
});

const [a, b, c, d, e] = numbers;

let reduceNumber = () => {
	console.log(a + b + c + d + e)
};

reduceNumber();


//  E


	class Dog{
		constructor(name, age, breed){
			this.name =  name;
			this.age = age;
			this.breed = breed;
		}
	}

	const newDog = new Dog("Frankie", 5, "Miniature Dachshund");

	console.log(newDog);
	